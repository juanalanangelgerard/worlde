fun comparator(wordToGuess : String, position : Int, letter:Char): Status{
    if (wordToGuess[position]==letter){
        return Status.CORRECT
    } else{
        for (z in 0..4){
            if (wordToGuess[z]==letter){
                return Status.ANOTHERPOSSITION
            }
        }
    }
    return Status.INCORRECT
}

