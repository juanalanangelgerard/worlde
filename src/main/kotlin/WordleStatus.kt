data class WordleStatus(val userWord: String, val secretWord: String) {
    val listWordStatus = listOf<Status>(comparator(secretWord, 0, userWord[0]), comparator(secretWord, 1, userWord[1]), comparator(secretWord, 2, userWord[2]), comparator(secretWord, 3, userWord[3]), comparator(secretWord, 4, userWord[4]))

    val isCorrect get(): Boolean = theWorldIsCorrect(listWordStatus[0],listWordStatus[1],listWordStatus[2],listWordStatus[3],listWordStatus[4])
}

enum class Status { CORRECT, INCORRECT, ANOTHERPOSSITION }

fun theWorldIsCorrect
            (firstLetterStatus: Status, secondLetterStatus: Status,
             thirdLetterStatus: Status, fourthLetterStatus: Status,
             fifthLetterStatus: Status): Boolean {
    return firstLetterStatus== Status.CORRECT && secondLetterStatus== Status.CORRECT && thirdLetterStatus == Status.CORRECT&& fourthLetterStatus== Status.CORRECT && fifthLetterStatus== Status.CORRECT
}
