import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import kotlin.io.path.Path


data class Game( var roundCount: Int = 1, var playerWin: Boolean = false, var textLenguage : TextLenguage) {


    fun randomWord(wordleListCat: List<String>): String {
        val secretWord = wordleListCat.random()
        return secretWord
    }

    fun start() {
        val wordleListCat : List<String> =  when (textLenguage.lenguage){
            Lenguage.CATALONIAN -> Files.readAllLines(Paths.get("src/main/resources/wordleDictionary_ca_ES.txt"))
            Lenguage.SPANISH -> Files.readAllLines(Paths.get("src/main/resources/wordleDictionary_es_ES.txt"))
        }
        val secretWord = randomWord(wordleListCat)
        playWord(secretWord,wordleListCat)
    }
    fun playWord(secretWord: String, list: List<String>) {

        println(textLenguage.askWorldText())


        while (roundCount < 7 && !playerWin) {
            val userWord = askUserWord(list)
            playerWin = round(userWord, secretWord)
            roundCount++
        }
        if (playerWin) {
            println(textLenguage.winOrLoose(true))
        } else println(textLenguage.winOrLoose(false))

        estadisticsOperations(secretWord,playerWin,roundCount-1,textLenguage)

        roundCount = 1
        playerWin = false

    }

    fun askUserWord(list: List<String>): String{
        var userWord: String = ""
        val scanner = Scanner(System.`in`)
        while (!validator(userWord, list)) {
            userWord = scanner.nextLine()
            if (!validator(userWord, list)) println(textLenguage.incorrectEntry()) else break
        }
        return userWord
    }

    fun changeLeanguage() {
        val scanner = Scanner(System.`in`)
        print(textLenguage.lenguageMenu())
        when(textLenguage.lenguage){
            Lenguage.CATALONIAN -> when(scanner.nextLine()) {
                "1" -> textLenguage.lenguage = Lenguage.SPANISH
                else -> textLenguage.lenguage = Lenguage.SPANISH
            }
            Lenguage.SPANISH -> when(scanner.nextLine()) {
                "1" -> textLenguage.lenguage = Lenguage.CATALONIAN
                else -> textLenguage.lenguage = Lenguage.CATALONIAN
            }
        }
    }
}

fun play(){
    val scanner = Scanner(System.`in`)
    val game = Game(textLenguage = TextLenguage())
    println(game.textLenguage.textMenu())
    val askMenu = scanner.nextLine()
    when (askMenu) {
        "1" -> game.start()
        "2" -> game.changeLeanguage()
    }
    while (askMenu!="3") {
        println(game.textLenguage.textMenu())
        when (scanner.nextLine()) {
            "1" -> game.start()
            "2" -> game.changeLeanguage()
            "3" -> break
        }
    }
}

fun main() {
    play()
}




