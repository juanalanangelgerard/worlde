/**
 * Validates if the word is valid according to the list
 */
fun validator(userEntry : String, listOfAvialable: List<String>): Boolean{
    var userEntry = userEntry.lowercase()
    userEntry = userEntry.replace("[^a-zñç]".toRegex(), "")
    return userEntry.length==5 && userEntry in listOfAvialable
}

