enum class Lenguage {
    SPANISH, CATALONIAN
}
data class TextLenguage(var lenguage: Lenguage = Lenguage.CATALONIAN){
    fun askWorldText(): String{
        return when(lenguage){
            Lenguage.CATALONIAN -> "Escrigui una paraula de 5 lletres"
            Lenguage.SPANISH -> "Escriba una palabra de 5 letras"
        }
    }
    fun incorrectEntry(): String{
        return when(lenguage){
            Lenguage.CATALONIAN->"La paraula no té els requisits necessaris, provi una altra vegada"
            Lenguage.SPANISH-> "La palabra introducida no tiene los requisitos necesarios, pruebe otra vez"
        }
    }
    fun winOrLoose(boolean: Boolean): String{
        return when(boolean){
            true -> when(lenguage){
                Lenguage.CATALONIAN -> "Has guanyat"
                Lenguage.SPANISH -> "Has ganado"
            }

            false -> when(lenguage){
                Lenguage.CATALONIAN -> "Has perdut"
                Lenguage.SPANISH -> "Has perdido"
            }
        }
    }
    fun textMenu(): String{
        return when(lenguage){
            Lenguage.CATALONIAN-> "1.Jugar una partida\n" +
                    "2.Cambiar l'idioma\n" +
                    "3.Sortir\n"
            Lenguage.SPANISH -> "1.Jugar una partida\n" +
                    "2.Cambiar el idioma\n" +
                    "3.Salir\n"
        }
    }

    fun lenguageMenu(): String{
        return when(lenguage){
            Lenguage.CATALONIAN-> "1.Cambiar a Castellà\n"
            Lenguage.SPANISH-> "1.Cambiar a Catalan\n"
        }
    }

    fun textEstadistics(type: Boolean = false): String{
        return if (type) {
            when(lenguage){
                Lenguage.CATALONIAN -> "ENCERTADES"
                Lenguage.SPANISH -> "ACERTADAS"
            }
        }else{
            when(lenguage){
                Lenguage.CATALONIAN -> "ERRADES"
                Lenguage.SPANISH -> "ERRADAS"
            }
        }
    }
}
