import java.lang.Exception


/** Prints the [message] in a specific [status] that the video-game "Wordle" uses to check the position of a letter.
 * Allowed colors are green, yellow, and grey (or gray). (Case-insensitive).
 * @throws Exception If a [status] other than those indicated above is entered.
 * @return the text with the required ANSI escape codes to be displayed in color on the terminal. */
fun setTextColor(message: String, status: Status): String{

    val boldText = "\u001b[1m" //Efecte text negreta
    val restartText = "\u001B[0m" //Reiniar estil de text
    val setcolor = when(status){
        Status.CORRECT -> "\u001b[38;2;106;170;100m"
        Status.ANOTHERPOSSITION -> "\u001b[38;2;201;180;88m"
        Status.INCORRECT -> "\u001b[38;2;120;124;126m"
        else -> throw Exception("Illegal color. Allowed colors: Green, yellow, and grey (or gray)")
    }

    return (boldText + setcolor + message + restartText)
}