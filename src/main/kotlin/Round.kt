import java.util.Scanner

fun round(userWord: String, secretWord: String)
        : Boolean {
    val statusWordleStatus = WordleStatus(userWord, secretWord)
    configureColorsWord(statusWordleStatus)
    return statusWordleStatus.isCorrect
}

fun configureColorsWord(statusWordleStatus: WordleStatus) {
    for (position in statusWordleStatus.listWordStatus.indices){
        print(setTextColor(statusWordleStatus.userWord[position].toString(), statusWordleStatus.listWordStatus[position]))
    }
    println()

}
