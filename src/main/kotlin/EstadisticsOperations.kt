import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import java.nio.file.Path
import java.nio.file.StandardOpenOption
import kotlin.io.path.Path
import kotlin.io.path.readText
import kotlin.io.path.writeText

@Serializable
@SerialName("estadistics")
data class Estadistics
    (val listAsserted: List<AssertedWord>, @SerialName("failed_word")val listFailed: List<String>)

@Serializable
@SerialName("asserted_word")
data class AssertedWord(val word: String, val round: Int)


fun estadisticsOperations(secretWord: String, playerWin: Boolean, roundCount: Int, textLenguage: TextLenguage) {
    val listAsserted = mutableListOf<AssertedWord>()
    val listFailed = mutableListOf<String>()

    if (playerWin) listAsserted += AssertedWord(secretWord,roundCount)
    else listFailed += secretWord

    val path: Path = when (textLenguage.lenguage){
        Lenguage.CATALONIAN -> Path("src/main/resources/estadistics_ca.xml")
        Lenguage.SPANISH -> Path("src/main/resources/estadistics_es.xml")
        else -> Path("src/main/resources/estadistics_ca.xml")
    }

    if (checkPath(path)) {
        val estadistics = readEstadisticsFromFile(path)
        estadistics.listAsserted.forEach { listAsserted += it }
        estadistics.listFailed.forEach { listFailed += it }
        val estadisticsXml = createEstadisticsXml(listAsserted, listFailed)
        writeInPath(path, estadisticsXml)
    }else createPath(path,createEstadisticsXml(listAsserted, listFailed))

    showEstadistics(listAsserted, listFailed, textLenguage)

}

fun showEstadistics(
    listAsserted: MutableList<AssertedWord>,
    listFailed: MutableList<String>,
    textLenguage: TextLenguage
) {
    println(textLenguage.textEstadistics(true))
    listAsserted.forEach { println("${it.word} -- ${it.round}") }
    println(textLenguage.textEstadistics())
    listFailed.forEach { println(it) }
}

private fun readEstadisticsFromFile(path: Path): Estadistics {
    val xml = path.readText()
    return XML.decodeFromString(xml)
}

private fun createEstadisticsXml(listAsserted: MutableList<AssertedWord>, listFailed: MutableList<String>): String{
    val newEstadistics = createEstadistics(listAsserted, listFailed)
    return XML.encodeToString(newEstadistics)
}

private fun createEstadistics
            (listAsserted: List<AssertedWord>, listFailed: List<String>)
= Estadistics(listAsserted,listFailed)


fun checkPath(path: Path) = path.toFile().exists()

fun createPath(path: Path, xmlData: String) {
    path.writeText(xmlData, options = arrayOf(StandardOpenOption.CREATE))
}

fun writeInPath(path: Path, xmlData: String) {
    path.writeText(xmlData, options = arrayOf(StandardOpenOption.WRITE))
}

fun appendInPath(path: Path, xmlData: String) { //no se usa, pero lo dejo por si nos sirve
    path.writeText(xmlData, options = arrayOf(StandardOpenOption.APPEND))
}