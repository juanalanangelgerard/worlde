import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.nio.file.Files
import java.nio.file.Paths
import java.util.Scanner

internal class MainKtTest {

    @Test
    fun start() {
        val scanner = Scanner(System.`in`)
        val userEntry = "estic"
        val secretWord = "estic"
        val wordleListCat : List<String> = Files
            .readAllLines(Paths.get("src/main/resources/wordleDictionary_ca_ES.txt"))
        val func = round(userEntry, secretWord)
        assertTrue(func)

    }
}