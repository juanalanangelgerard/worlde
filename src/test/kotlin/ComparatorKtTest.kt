
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ComparatorKtTest {

    @Test
    fun comparatorCorrectTest() {
        val letter = 'e'
        val secretWord = "estic"
        val status = Status.CORRECT
        val comparator = comparator(secretWord, 0, letter)
        assertEquals(status, comparator)
    }



    @Test
    fun comparatorAnotherTest(){
        val letter = 'i'
        val secretWord = "estic"
        val status = Status.ANOTHERPOSSITION
        val comparator = comparator(secretWord, 0, letter)
        assertEquals(status,comparator)
    }

    @Test
    fun comparatorIncorrectTest(){
        val letter = 'q'
        val secretWord = "estic"
        val status = Status.INCORRECT
        val comparator = comparator(secretWord, 0, letter)
        assertEquals(status,comparator)
    }
}