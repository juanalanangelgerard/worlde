import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class WordleStatusTest {

    @Test
    fun getFirstLetterTest() {
        val wordleStatus1 = WordleStatus("caspa", "porta")
        assertEquals(Status.INCORRECT,wordleStatus1.listWordStatus[0])
    }

    @Test
    fun getSecondLetterTest() {
        val wordleStatus1 = WordleStatus("caspa", "porta")
        assertEquals(Status.ANOTHERPOSSITION,wordleStatus1.listWordStatus[1])
    }

    @Test
    fun getThirdLetterTest() {
        val wordleStatus1 = WordleStatus("caspa", "porta")
        assertEquals(Status.INCORRECT,wordleStatus1.listWordStatus[2])
    }

    @Test
    fun getFourthLetterTest() {
        val wordleStatus1 = WordleStatus("caspa", "porta")
        assertEquals(Status.ANOTHERPOSSITION,wordleStatus1.listWordStatus[3])
    }

    @Test
    fun getFiveLetterTest() {
        val wordleStatus1 = WordleStatus("caspa", "porta")
        assertEquals(Status.CORRECT,wordleStatus1.listWordStatus[4])
    }

    @Test
    fun isCorrect() {
        val wordleStatus1 = WordleStatus("caspa", "porta")
        assertEquals(false, wordleStatus1.isCorrect)
        val wordleStatus2 = WordleStatus("porta", "porta")
        assertEquals(true, wordleStatus2.isCorrect)
    }
}