import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class EntryValidatorKtTest {

    @Test
    fun validatortesttrue() {
        val userEntry = "EsTiC"
        val listOfAvialable = listOf<String>("estic", "pluja")
        val validator = validator(userEntry, listOfAvialable)
        assertTrue(validator)
    }
    @Test
    fun validatortestfalse() {
        val userEntry = "EsTiCa"
        val listOfAvialable = listOf<String>("estic", "pluja")
        val validator = validator(userEntry, listOfAvialable)
        assertFalse(validator)
    }
    @Test
    fun validatortestRegex() {
        val userEntry = "                                             e                    /&%$             S               =??          T           ¿¿¿¿            I       C   -.,.-:-,,.-,"
        val listOfAvialable = listOf<String>("estic", "pluja")
        val validator = validator(userEntry, listOfAvialable)
        assertTrue(validator)
    }
    @Test
    fun validatorLength() {
        val userEntry = "3stic"
        val listOfAvialable = listOf<String>("estic", "pluja")
        val validator = validator(userEntry, listOfAvialable)
        assertFalse(validator)
    }
}